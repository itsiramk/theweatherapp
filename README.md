# the_weather_app

This project demonstrates the use of external weather API's in flutter.

We are using the OpenWeather api with the help of a API key.

Spinner is used to show loading of screens.

We have also used Navigation to and fro in screens and passing data to a state object.

![weatherapp](images/weather.PNG)
